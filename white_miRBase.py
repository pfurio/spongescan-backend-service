# -*- coding: utf-8 -*-

# PROXY - Sockets
#from httplib2 import socks
#import socket

import time

import urllib
import gzip

import re

import getopt, sys, os

URL="ftp://mirbase.org/pub/mirbase/CURRENT/miRNA.dat.gz"
# Whitelist tags
TAGS=["cloned","Northern","PCR","RT-PCR","qRT-PCR","5'RACE","RTPCR","in-situ","qPCR","miRAP-cloned","3'RACE","insitu","RACE","miRAP","primer-extension","RAKE"]


def main():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "s:k:o:", ["species=", "kmers=", "output="])
    except getopt.GetoptError as err:
        print(err) # will print something like "option -a not recognized"
        usage()
        sys.exit(2)
    
    outfile = kmer = species = None

    
    for o, a in opts:
        if o in ("-s", "--species"):
            species = a
        elif o in ("-k", "--kmers"):
            kmer = int(a)
        elif o in ("-o", "--output"):
        	outfile = a
        else:
            assert False, "Unhandled option"
    
    if species is not None and kmer is not None and outfile is not None:
        get_miRNA_sequences(species, kmer, outfile)
    else:
        usage()


def usage():
    print "\nUsage: python miRBase_whitelist.py <mandatory>"
    print "\t-s, --species:\n\t\t Species from which extract miRNAs. 'all' will extract miRNAs from all the species"
    print "\t-k, --kmer:\n\t\t Kmer size to get from selected miRNAs"
    print "\t-o, --output:\n\t\t Output file"
    print "\n16/03/2015. Pedro Furió Tarí.\n"


def rev_compl(sequence):

	new_sequence = ""
	for i in reversed(sequence):
		if i == "A":
			new_sequence = new_sequence + "T"
		elif i == "T":
			new_sequence = new_sequence + "A"
		elif i == "G":
			new_sequence = new_sequence + "C"
		elif i =="C":
			new_sequence = new_sequence + "G"
	return new_sequence


def get_miRNA_sequences(species, kmer, outfile):

	myfile = open(outfile, 'w')

	# Download the file if the file is older than ~ one month
	if os.path.isfile("miRNA.dat.gz") is False or time.time() - os.path.getmtime("miRNA.dat.gz") > 2000000:
		urllib.urlretrieve(URL, "miRNA.dat.gz")

	filein = gzip.open("miRNA.dat.gz",'r')

	mirna_name = ""
	species_tag = False
	sequence_tag = False
	exp_tag = False
	position = 0

	to_report = []

	for line in filein:
		line_split = line.split()

		if line_split[0] == "DE":
			
			# Here we have the definition of the species and miRNA name
			if species == "all" or species.lower() in line.lower():
				species_tag = True
			else:
				species_tag = False
			
		if species_tag is True and line_split[0] == "FT":

			if "product" in line:
				mirna_name = line.split('"')[1]

			if line_split[1] == "miRNA":
				# Here we have the position of the current miRNA
				position = int(line_split[2].split("..")[0])

			if "/experiment" in line:
				if line[-2] != '"':
					exp_line = line
					exp_tag = True
				else:
					exp_tag = False
					for tag in TAGS:
						if tag in line:
							to_report.append([mirna_name, position])
							break

			if exp_tag is True:
				exp_line = exp_line + line

				if line[-2] == '"':
					exp_tag = False
					for tag in TAGS:
						if tag in exp_line:
							to_report.append([mirna_name, position])
							break

		if sequence_tag is True:
			if line[0] != "/":
				sequence = sequence + "".join(line_split[:-1])
			else:
				sequence = sequence.replace("u","t")
				# Analyze each miRNA in to_report
				for mi_mirna in to_report:
					pos = mi_mirna[1]
					myfile.write(rev_compl(sequence[pos:(pos+kmer)].upper()) + "\tcanonical\t" + mi_mirna[0] + "\t\n")
				to_report = []
				sequence_tag = False

		if species_tag is True and line_split[0] == "SQ":
			sequence_tag = True
			sequence = ""

	myfile.close()
	os.system("sort " + outfile + " | uniq > " + outfile + ".tmp")
	os.system("mv " + outfile + ".tmp " + outfile)
	
if __name__ == "__main__":
    main()
