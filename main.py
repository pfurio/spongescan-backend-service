# -*- coding: utf-8 -*-

import site
site.addsitedir('/data/conesa/spongescan/python/2.6/lib/python2.6/site-packages') # Florida deployment

import os
import re        # Regular expressions
import ftplib    # FTP connections
import socket
# from pymongo import MongoClient
# from bson.json_util import dumps, loads

from flask import Flask, request, redirect, url_for, jsonify, make_response, current_app, Response

from werkzeug import secure_filename
from datetime import timedelta
from functools import update_wrapper
from httplib2 import socks              # PROXY

# ID GENERATOR
import uuid

# My imports
import myfunctions
import jobs

PROXY_HOST   = "e-imgsrv.ufhpc"
PROXY_PORT   = 3128
PROXY_ENABLE = True

FILES_PATH  = '/data/conesa/spongescan/jobs/uploads'
GTF_EXTENSIONS = set(['gtf', 'gff'])
FASTA_EXTENSIONS = set(['fa', 'fasta'])

app = Flask(__name__)
app.config['DEBUG'] = True
#app.config['THREADED'] = True
app.config['UPLOAD_FOLDER'] = FILES_PATH
# The maximum allowed payload will be 2 GBs
app.config['MAX_CONTENT_LENGTH'] = 2 * 1024 * 1024 * 1024

# def createJobs(job_id, email, ):
#     # 1. Send e-mail to the researcher
#     subject  = "lncScan: New job launched"
#     msg_body =  "The job " + job_id + " was launched successfully. We will send you another e-mail when the process finishes."
#     s_email.send_email(email, subject, msg_body)
#
#     # 2. Submit jobs
#     job_ids = []
#     for kmer in range(6,8):
#         job_ids.append( send_job.create_job( command , args, stdout ) )
#
#     # Wait for the jobs to finish
#     retVals = send_job.waitForJobs(job_ids)
#
#     for value in retVals:
#         if value == 0:
#             # TODO: Something went badly
#             # Send an email informing the user and remove data from the database
#             return
#
#     # Upgrade DB with current status of job_id
#     upgrade_db()
#
#     job_ids = []
#     # send_job_GTF
#     # send_job_conservation
#

def allowed_file(filename, allowed_extensions):
    return '.' in filename and \
           filename.rsplit('.')[-1].lower() in allowed_extensions

################

# UPLOAD

###############

def validate_fields(request):

    valid = True

    try:
        # Obtain all the possible arguments
        gtf_sel       = request.form['gtf_sel']       # ensembl or upload (combobox)
        fasta_sel     = request.form['fasta_sel']     # ensembl or upload (combobox)
        release_gtf   = request.form['release']       # Release number of the selected Ensembl GTF file
        species_gtf   = request.form['species']       # Species name of the selected Ensembl GTF file
        ens_gtf       = request.form['ens_gtf']       # Selected Ensembl GTF file to download
        ens_fasta     = request.form['ens_fasta']     # Selected Ensembl FASTA file to download
        use_cons      = request.form['use_cons']      #
        cons_db       = request.form['cons_db']       #
        lod_score     = request.form['lod_score']     # LOD Score value to run the prediction
        kmer_complex  = request.form['kmer_complex']  # Kmer complexity threshold to run the prediction
        sd            = request.form['sd']            # Standard deviation threshold to run the prediction
        total_sites   = request.form['total_sites']   # Minimum total number of binding sites threshold to run the prediction
        wobbles       = request.form['wobbles']       # Boolean to know whether use wobbles or not to run the prediction
        reverse_fasta = request.form['reverse_fasta'] # The negative strand of the fasta file is already complementary reversed?
        mail          = request.form['mail']          # Mail used to notify the user about the running
        mirna         = request.form['mirnas']        # miRNA species to extract the canonical sequences
    except:
        valid = False
        message = "Some of the required arguments were not found in the query.\n"

    if valid is True:

        message = ""
        # 1. Check if not selected GTF from the combobox
        if gtf_sel == 'ensembl' and ens_gtf == "null":
            valid = False
            message = "You must select one of the GTF's from ensembl or upload a GTF file.\n"

        if gtf_sel == 'upload' and ('upload_gtf_filefield' not in request.files or allowed_file(request.files['upload_gtf_filefield'].filename, GTF_EXTENSIONS) == False):
            message += "You haven't put any GTF file or the format is incorrect.\n"
            valid = False

        # 2. Check fasta file
        if fasta_sel == 'ensembl' and ens_fasta == "null":
            valid = False
            message += "You must select one of the FASTA files from ensembl or upload your own FASTA file.\n"

        if fasta_sel == 'upload' and ('upload_fasta_filefield' not in request.files or allowed_file(request.files['upload_fasta_filefield'].filename, FASTA_EXTENSIONS) == False):
            message += "The FASTA file you have uploaded doesn't have the proper extension. Please, check it.\n"
            valid = False

        # 3. Check conservation
        if int(use_cons) == 1 and cons_db == "null":
            message += "You must select a conservation database if you want to see it with conservation data also. Otherwise, deselect it on step 4.\n"
            valid = False

        # 4. Check mail
        if mail != "null" and len(mail) > 0 and not re.match(r"[^@]+@[^@]+\.[^@]+",mail):
            message += "The input e-mail is not valid. Please, check it.\n"
            valid = False

        if mirna == "null":
            valid = False
            message += "You must select one miRNA species from the combobox.\n"

    return [valid, message]


@app.route('/submit', methods=['POST'])
def submit_job():
    [success, msg] = validate_fields(request)
    salida = None

    # Obtain all the possible arguments
    try:
        gtf_sel       = request.form['gtf_sel']
        fasta_sel     = request.form['fasta_sel']
        release_gtf   = request.form['release']
        species_gtf   = request.form['species']
        ens_gtf       = request.form['ens_gtf']
        ens_fasta     = request.form['ens_fasta']
        use_cons      = request.form['use_cons']
        cons_db       = request.form['cons_db']
        lod_score     = request.form['lod_score']
        kmer_complex  = request.form['kmer_complex']
        sd            = request.form['sd']
        total_sites   = request.form['total_sites']
        wobbles       = request.form['wobbles']
        reverse_fasta = request.form['reverse_fasta']
        mail          = request.form['mail']
        mirna         = request.form['mirnas']
    except:
        msg = "Internal error: The form couldn't be parsed properly"
        success = False


    if success is True:

        # Generate ID
        myid = str(uuid.uuid4())[:8]
        while myfunctions.existsJob(myid) is True:
            myid =  str(uuid.uuid4())[:8]

        outdir = FILES_PATH + "/" + str(myid)
        os.makedirs(outdir)

        download_gtf   = True # flag to know if we have to download the gtf file or not
        download_fasta = True # flag to know if we have to download the fasta file or not
        gtf_file = ""
        fasta_file = ""

        # GTF FILE
        if gtf_sel == 'upload':
            # Download GTF
            download_gtf = False
            file = request.files['upload_gtf_filefield']
            filename = secure_filename(file.filename)
            gtf_file = os.path.join(outdir, filename)
            file.save(gtf_file)
        else:
            # Create gtf file url
            gtf_file = "ftp.ensembl.org/pub/" + release_gtf + "/gtf/" + species_gtf + "/" + ens_gtf

        # FASTA FILE
        if fasta_sel == 'upload':
            # Download FASTA
            download_fasta = False
            file = request.files['upload_fasta_filefield']
            filename = secure_filename(file.filename)
            fasta_file = os.path.join(outdir, filename)
            file.save(fasta_file)
        else:
            # Create fasta file url
            fasta_file = "ftp.ensembl.org/pub/" + release_gtf + "/fasta/" + species_gtf + "/ncrna/" + ens_fasta

        if 'upload_expr_lncrna_filefield' in request.files:
            file = request.files['upload_expr_lncrna_filefield']
            filename = secure_filename(file.filename)
            lncexpr_file = os.path.join(outdir, filename)
            file.save(lncexpr_file)
        else:
            lncexpr_file = None

        if 'upload_expr_mirna_filefield' in request.files:
            file = request.files['upload_expr_mirna_filefield']
            filename = secure_filename(file.filename)
            mirnaexpr_file = os.path.join(outdir, filename)
            file.save(mirnaexpr_file)
        else:
            mirnaexpr_file = None

        expression = [lncexpr_file, mirnaexpr_file]


        if jobs.submit_new_job(myid, download_gtf, download_fasta, gtf_file, fasta_file, lod_score, kmer_complex, sd, total_sites, wobbles, reverse_fasta, species_gtf, mail, mirna, expression) is True:
            salida = jsonify(success= True, msg=msg, jobId=myid)
        else:
            jobs.send_email("pedrofurio@gmail.com","lncScan ERROR", "Hay problemas con el envío de jobs !!")
            salida = jsonify(success= False, msg="There was an internal problem launching the jobs.")
            salida.status_code = 400
    else:

        salida = jsonify(success= False, msg=msg)
        salida.status_code = 400
    return salida
    # return jsonify(success= True, msg = "kkkdelavak", jobId="mus musculus")




# kmer_size is the size of the kmers we want to show
# known will be True or False. If true, it will report all known miRNA binding sites, otherwise, it will report other kmer sequence binding sites
@app.route('/getArguments/<job_id>')
def getArguments(job_id):
    return jsonify(myfunctions.getArguments(job_id))


# kmer_size is the size of the kmers we want to show
# known will be True or False. If true, it will report all known miRNA binding sites, otherwise, it will report other kmer sequence binding sites
@app.route('/getJob/<job_id>/<kmer_size>/<known>')
def get_job(job_id, kmer_size, known):
    start  = int(request.args.get('start'))
    limit  = int(request.args.get('limit'))
    tosort = request.args.get('sort')
    direct = request.args.get('dir')
    filter = request.args.get('filter')
    return jsonify(myfunctions.retreiveJob(job_id, kmer_size, known, start, limit, tosort, direct, filter))

@app.route('/jobStatus/<job_id>')
def getJobStatus(job_id):
    salida = myfunctions.jobStatus(job_id)
    myjson = jsonify(salida)
    if salida['success'] is False:
        myjson.status_code = 400
    return myjson


@app.route('/getEnsemblCDNAs', methods=['GET'])
def getAvailableCDNAs():

    if PROXY_ENABLE is True:
        socks.setdefaultproxy(socks.PROXY_TYPE_HTTP, PROXY_HOST, PROXY_PORT)
        socket.socket = socks.socksocket

    release = request.args.get('release')
    species = request.args.get('species')

    server = "ftp.ensembl.org"
    remote = ftplib.FTP(server)
    remote.login()
    path = "ftp://" + server + "/pub/" + release + "/fasta/" + species + "/ncrna/"
    # Get GTF files available
    cdnas_list = remote.nlst("pub/" + release + "/fasta/" + species + "/ncrna")

    myfastas = []

    for i in range(len(cdnas_list)):
        mystring = cdnas_list[i].split("/")[-1]
        mysearch = re.search('fa.gz',mystring)
        if mysearch is not None:
            myfastas.append( {"name": mystring.split(".fa.gz")[0], "value": mystring, "path": path + mystring} )

    response = myfastas

    remote.close()

    return jsonify({"success": True, "response": response})


@app.route('/getEnsemblGTFs', methods=['GET'])
def getAvailableGTF():

    if PROXY_ENABLE is True:
        socks.setdefaultproxy(socks.PROXY_TYPE_HTTP, PROXY_HOST, PROXY_PORT)
        socket.socket = socks.socksocket

    release = request.args.get('release')

    server = "ftp.ensembl.org"
    remote = ftplib.FTP(server)
    remote.login()
    path = "ftp://" + server

    if release == "all":

        list_pub = remote.nlst("pub")
        releases = []

        for i in list_pub:
            mysearch = re.search('release',i)
            if mysearch is not None:
                releases.append(int(i.split("-")[1]))

        maximo = max (releases)
        salida = []

        for i in range(maximo, 49, -1):
            salida.append( {"name": "release-" + str(i), "value": "release-" + str(i)} )

        response = salida

    else:
        path = path + "/pub/" + release
        species = request.args.get('species')

        if species == "all":
            genomes = remote.nlst("pub/" + release + "/gtf")

            for i in range(len(genomes)):
                genomes[i] = {"name": genomes[i].split("/")[-1], "value": genomes[i].split("/")[-1]}

            response = genomes

        else:
            path = path + "/gtf/" + species + "/"
            # Get GTF files available
            gtfs_list = remote.nlst("pub/" + release + "/gtf/" + species)

            mygtfs = []

            for i in range(len(gtfs_list)):
                mystring = gtfs_list[i].split("/")[-1]
                mysearch = re.search('gtf',mystring)
                if mysearch is not None and "abinitio" not in mystring:
                    mygtfs.append( {"name": mystring.split(".gtf")[0], "value": mystring, "path": path + mystring} )

            response = mygtfs

    remote.close()

    return jsonify({"success": True, "response": response})


# Return kmers in a specified region
@app.route('/getKmers/<job_id>/<positions>/<kmer_size>')
def getKmers(job_id, positions,kmer_size):
    return jsonify(myfunctions.getRegions(job_id, positions,kmer_size))

@app.route('/getExpression/<job_id>/<gene_ids>')
def getExpression(job_id, gene_ids):
    return jsonify(myfunctions.getExpression(job_id, gene_ids))

@app.route('/download/<job_id>/<kmer_size>/<known>')
def download(job_id, kmer_size, known):
    myHeader = {}
    if known == "1":
        myHeader["Content-Disposition"] = "attachment; filename=" + job_id + "_" + kmer_size + "_KnownMirnas.csv"
    else:
        myHeader["Content-Disposition"] = "attachment; filename=" + job_id + "_" + kmer_size + "_UnknownMirnas.csv"
       
    return Response(myfunctions.createCSV(job_id, kmer_size, known), 
           headers=myHeader, mimetype='text/csv')
    #response = make_response(myfunctions.createCSV(job_id, kmer_size, known, tosort, direct, filter))
    # This is the key: Set the right header for the response
    # to be downloaded, instead of just printed on the browser
    # response.headers["Content-Disposition"] = "attachment; filename=" + job_id + "_" + kmer_size + ".csv"
    #return response


