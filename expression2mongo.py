# -*- coding: utf-8 -*-

import site
site.addsitedir('/data/conesa/spongescan/python/2.6/lib/python2.6/site-packages') # Florida deployment

import re
import json
from pymongo import MongoClient
import getopt, sys, os

def main():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "b:e:j:", ["biotype=", "expression=", "jobID="])
    except getopt.GetoptError as err:
        print(err) # will print something like "option -a not recognized"
        usage()
        sys.exit(2)
    
    biotype = expression = jobid = None
    
    for o, a in opts:
        if o in ("-b", "--biotype"):
            if a == 'lncRNA' or a == 'miRNA':
                biotype = a
            else:
                print 'The biotype inserted is not correct. Can only be one of lncRNA or miRNA.'
                sys.exit(2)
        elif o in ("-e", "--expression"):
            expression = a
        elif o in ("-j", "--jobID"):
            jobid = a
        else:
            assert False, "Unhandled option"

    if biotype is not None and expression is not None and jobid is not None:
        insertExpression(jobid, expression, biotype)
    else:
        usage()


def usage():
    print "\nUsage: python expression2mongo.py <mandatory>"
    print "\t-j, --jobID:\n\t\t Job ID to associate the expression information with"
    print "\t-b, --biotype:\n\t\t Biotype (lncRNA or miRNA)"
    print "\t-e, --expression:\n\t\t Expression file"
    print "\n12/11/2015. Pedro Furió Tarí.\n"

def insertExpression(jobid, expression, biotype):
    # Expression file
    mi_file = open(expression,'r')

    linea = mi_file.next()[:-1]
    if "#Description" not in linea:
        print >> sys.stderr, "Description tag could not be found in the file."
        sys.exit(1)
    description = linea.split('\t')[1]

    linea = mi_file.next()[:-1]
    if "#Tissue" not in linea:
        print >> sys.stderr, "Tissue tag could not be found in the file."
        sys.exit(1)
    tissues = linea.split('\t')[1:]

    linea = mi_file.next()[:-1]
    if "#Sample" not in linea:
        print >> sys.stderr, "Sample tag could not be found in the file."
        sys.exit(1)
    samples = linea.split('\t')[1:]


    # Database connection
    credentials = json.load(open(os.path.dirname(__file__) + "/mongo_credentials.json",'r'))
    client = MongoClient(credentials['host'])
    db = client[credentials['database']] #base de datos a usar
    user = credentials['users']['admin']['user']
    pswd = credentials['users']['admin']['pass']
    db.authenticate(user, pswd)

    all_mirnas = db["sponges"].distinct("mirnas", {"job_id": jobid})
    result = {"job_id": jobid, "biotype": biotype, "expr_format": description, "expr_fields": samples, "tissues": tissues, "genes": []}

    for line in mi_file:
        expr = line.split()
        gene_id = expr[0]

        annot_result = db['sponges'].find_one({"job_id": jobid, "gene_id": gene_id})
        if annot_result is not None or gene_id in all_mirnas:
            result['genes'].append({"gene_id": gene_id, "expr_values": [float(mi_expr) for mi_expr in expr[1:]]})

    expr_db = db['expression']
    expr_db.insert_one(result)


if __name__ == "__main__":
    main()
