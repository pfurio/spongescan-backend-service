# -*- coding: utf-8 -*-
# Author: Pedro Furió Tarí

import getopt
import sys
import os.path
import re

biotypes = [
    "3prime_overlapping_ncrna",
    "ambiguous_orf",
    "antisense",
    "lincRNA",
    "ncrna_host",
    "non_coding",
    "processed_transcript",
    "retained_intron",
    "sense_intronic",
    "sense_overlapping"
]

def main():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "i:o:", ["input=", "output="])
    except getopt.GetoptError as err:
        print(err) # will print something like "option -a not recognized"
        usage()
        sys.exit(2)

    ifile = None
    ofile = None
    
    for o, a in opts:
        if o in ("-i","--input"):
            if os.path.isfile(a):
                ifile = a
            else:
                sys.stderr.write("\nThe cdna file you have inputted is not correct.\n")
                usage()
                sys.exit()
        elif o in ("-o", "--output"):
            ofile = a
        else:
            assert False, "Unhandled option"

    if ifile is not None and ofile is not None:
        run(ifile, ofile)
    else:
        usage()


def usage():
    print "\nUsage: python cdna_parser.py [options] <mandatory>"
    print "Options:"
    print "\t-i, --input:\n\t\t CDNA fasta file"
    print "\t-o, --output:\n\t\t Output file with lncRNA parsed CDNA fasta file"
    print "\n08/04/2015. Pedro Furió Tarí.\n"


def run(ifile, ofile):

    my_ifile = open(ifile,'r')
    my_ofile = open(ofile,'w')

    toreport = False
    for linea in my_ifile:
        if linea[0] == ">":
            try:
                mybiotype = linea[re.search("transcript_biotype",linea).end()+1:].split()[0]
                if mybiotype in biotypes:
                    toreport = True
                    my_ofile.write(linea)
                else:
                    toreport = False
            except:
                toreport = False
                pass
        else:
            if toreport is True:
                my_ofile.write(linea)

    my_ifile.close()
    my_ofile.close()

if __name__ == "__main__":
    main()

