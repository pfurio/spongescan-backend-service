# -*- coding: utf-8 -*-

import sys
import os

# DATABASE
from time import gmtime, strftime
from pymongo import MongoClient

# EMAIL
import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.MIMEImage import MIMEImage

# JOBS
import drmaa

# PROXY - Sockets
from httplib2 import socks
import socket

# THREADS
import threading

# Download from FTP
import urllib

# To decompress GTF files
import gzip

# To remove directory and all the contents
import shutil

# Script to get miRNA list
from white_miRBase import get_miRNA_sequences

import json

credentials = json.load(open(os.path.dirname(__file__) + "/mongo_credentials.json",'r'))
db_user = credentials['users']['admin']['user']
db_pass = credentials['users']['admin']['pass']
host = credentials['host']
db   = credentials['database']

LOGS_PATH   = '/data/conesa/spongescan/jobs/logs'
FILES_PATH  = '/data/conesa/spongescan/jobs/uploads'

scripts_dir = "/data/conesa/spongescan/scripts"
predictor   = scripts_dir + "/predictor"
gtfparser   = scripts_dir + "/gtfparser"
cons        = "python " + scripts_dir + "/conservation.py"
expr2mongo  = "python " + os.path.dirname(__file__) + "/expression2mongo.py"

# PROXY CONFIGURATION
PROXY_HOST   = "e-imgsrv.ufhpc"
PROXY_PORT   = 3128
PROXY_ENABLE = True

# Lock for threads in order to let just one thread access DRMAA session
lock = threading.Lock()

# EMAIL
# JOB received
def send_email(toaddr, jobID, message, mytype):

    if len(toaddr) == 0: # If there is no mail inserted.
        return

    fromaddr = "lncscan.cipf@gmail.com"
    msg = MIMEMultipart('alternative')
    msg['From'] = fromaddr
    msg['To'] = toaddr

    if mytype == "newjob":
        msg['Subject'] = "spongeScan: New job received"

        text = "spongeScan\n\n\nYour job has been properly sent\nThis is your job ID: " + jobID + "\nAnother e-mail will be automatically sent once the results are ready.\n\nThanks for using spongeScan\n"
        html = """\
        <html>
          <head>
          <link href="http://fonts.googleapis.com/css?family=Roboto:300" rel="stylesheet" type="text/css">
          </head>
          <body>
            <img src="cid:logo" alt="spongeScan" style="width: 215px;"/> <br><br><br>
            <h1 style="Margin-top: 0;color: #6B5E5E;font-weight: 700;font-size: 34px;Margin-bottom: 18px;font-family: Roboto;line-height: 42px">Your job has been properly sent</h1>
            <p style="Margin-top: 0;color: #6B5E5E; font-family: Roboto,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px">
                This is your job ID: <strong style="font-weight: bold">""" + jobID + """</strong>
            </p>
            <p style="Margin-top: 0;color: #6B5E5E; font-family: Roboto,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px">
                Another e-mail will be automatically sent once the results are ready.
            </p></br></br>
            <p style="Margin-top: 0;color: #6B5E5E; font-family: Roboto,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px">
                Thanks for using spongeScan.
            </p>
          </body>
        </html>
        """

    elif mytype == "error":

        msg['Subject'] = "spongeScan: An error occurred"

        text = "spongeScan\n\n\nYour job with ID " + jobID + " has crashed.\n\nMessage error: " + message + "\n\nPlease, check that your input is correct. Otherwise, please resend us this e-mail.\n\nSorry for the inconveniences."
        html = """\
        <html>
          <head>
          <link href="http://fonts.googleapis.com/css?family=Roboto:300" rel="stylesheet" type="text/css">
          </head>
          <body>
            <img src="cid:logo" alt="spongeScan" style="width: 215px;"/> <br><br><br>
            <p style="Margin-top: 0;color: #6B5E5E; font-family: Roboto,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px">
                Your job with ID: <strong style="font-weight: bold">""" + jobID + """</strong> has crashed.
            </p>
            <p style="Margin-top: 0;color: #6B5E5E; font-family: Roboto,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px">
                Message error: """ + message + """
            </p>
            <p style="Margin-top: 0;color: #6B5E5E; font-family: Roboto,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px">
                Please, check that your input is correct. Otherwise, please resend us this e-mail.
            </p></br></br>
            <p style="Margin-top: 0;color: #6B5E5E; font-family: Roboto,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px">
                Sorry for the inconveniences.
            </p>
          </body>
        </html>
        """

    elif mytype == "finished":
        msg['Subject'] = "spongeScan: Your job is ready"

        text = "spongeScan\n\n\nYour job with ID " + jobID + " has finished. Please, go to the spongeScan webpage to see the results.\n\n\nThanks for using spongeScan\n"
        html = """\
        <html>
          <head>
          <link href="http://fonts.googleapis.com/css?family=Roboto:300" rel="stylesheet" type="text/css">
          </head>
          <body>
            <img src="cid:logo" alt="spongeScan" style="width: 215px;"/> <br><br><br>
            <p style="Margin-top: 0;color: #6B5E5E; font-family: Roboto,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px">
                Your job with ID """ + jobID + """ has finished. Please, go to the <a href="http://spongescan.rc.ufl.edu/">spongeScan webpage</a> to check the results.
            </p></br></br>
            <p style="Margin-top: 0;color: #6B5E5E; font-family: Roboto,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px">
                Thanks for using spongeScan.
            </p>
          </body>
        </html>
        """

    try:
        # Record the MIME types of both parts - text/plain and text/html.
        part1 = MIMEText(text, 'plain')
        part2 = MIMEText(html, 'html')

        msg.attach(part1)
        msg.attach(part2)

        # Open the logo
        fp = open("/data/conesa/spongescan/app/img/sponge_logo_215x50.png",'rb')
        msgImage = MIMEImage(fp.read())
        fp.close()

        msgImage.add_header('Content-ID','<logo>')
        msg.attach(msgImage)

        # Record the MIME types of both parts - text/plain and text/html.
        part1 = MIMEText(text, 'plain')
        part2 = MIMEText(html, 'html')

        s = smtplib.SMTP('localhost')
        s.sendmail(fromaddr, toaddr, msg.as_string())
        s.quit()
    except:
        print "Error: unable to send email"


# COMMAND LINES
def create_predictor_cmd(job_id, mirna, gtf, fasta, kmer_length, lod_score, kmer_complex, sd, total_sites, wobbles, reverse):

    # Mandatory:
    #     -m: miRNA tab file [kmer        canonical?      mirna   tissue(s)]
    #     -g: GTF file
    #     -f: Fasta file with sequences
    #     -j: Job ID
    #     -d: Database
    # Optional:
    #     -k: Kmer length [Default: 6]
    #     -t: LOD Score threshold [Default: 1]
    #     -c: Kmer Complexity Score threshold (Applied to kmers with no known miRNA [Default: 4])
    #     -s: Maximum standard deviation allowed. [Default: 10]
    #     -a: Minimum total binding sites detected threshold. [Default: 20]
    #     -w: Use wobbles [Default: Yes]
    #     -r: Fasta in negative strand in reverse complementary [Default: No]
    #     -h: Database host. Default: localhost
    #     -u: Database user
    #     -p: Database password

    my_cmd_line = predictor + " -m " + mirna + " -g " + gtf + " -f " + fasta + " -j " + job_id + " -k " + str(kmer_length)
    my_cmd_line = my_cmd_line + " -t " + str(lod_score) + " -c " + str(kmer_complex) + " -s " + str(sd) + " -a " + str(total_sites)
    my_cmd_line = my_cmd_line + " -h " + host + " -d " + db + " -u " + db_user + " -p " + db_pass

    if wobbles == "false":
        my_cmd_line = my_cmd_line + " -w"
    if reverse == "true":
        my_cmd_line = my_cmd_line + " -r"

    print my_cmd_line

    return my_cmd_line


def create_gtfparser_cmd(job_id, gtf, genome):

    # Mandatory:
    #     -g: GTF file
    #     -j: Job ID
    #     -a: Genome name: Human hg38, mus musculus mm10...
    #     -d: Database
    # Optional:
    #     -h: Database host:port. Default: localhost:27017
    #     -u: Database user
    #     -p: Database password

    my_cmd_line = gtfparser + " -j " + job_id + " -g " + gtf + " -a " + genome
    my_cmd_line = my_cmd_line + " -h " + host + " -d " + db + " -u " + db_user + " -p " + db_pass

    print my_cmd_line

    return my_cmd_line


def create_expression_cmd(job_id, biotype, expression):
    # Usage: python expression2mongo.py <mandatory>
    #     -j, --jobID:
    #          Job ID to associate the expression information with
    #     -b, --biotype:
    #          Biotype (lncRNA or miRNA)
    #     -e, --expression:
    #          Expression file

    my_cmd_line = expr2mongo + " -j " + job_id + " -b " + biotype + " -e " + expression
    print my_cmd_line
    return my_cmd_line


def create_conservation_cmd(job_id, species):
    # Mandatory:
    #     -s, --species:
    #              hsapiens, mmusculus...
    #     -j, --job_id:
    #              Job ID
    #     -d, --database:
    #              Database name
    #     -h, --host:
    #              Database host
    #     -u, --user:
    #              Database user
    #     -p, --pass:
    #              Database password

    my_cmd_line = cons + " -s " + species + " -j " + job_id
    my_cmd_line = my_cmd_line + " -h " + host + " -d " + db + " -u " + db_user + " -p " + db_pass

    print my_cmd_line

    return my_cmd_line


# DATABASE UPGRADE
def update_status(job_id, email, status, msg):

    # Get today's day
    day = strftime("%y%m%d")

    # Connect to database
    client = MongoClient(host)
    mydb  = client[db]
    mydb.authenticate(db_user, db_pass)

    result = mydb['queries'].find_one({"job_id": job_id})

    if result is None:
        # Insert
        mydb['queries'].insert({"job_id": job_id, "date": day, "email": email, "status": status, "msg": msg})
    else:
        # Update
        result['status'] = status
        result['msg'] = msg
        mydb['queries'].update({"job_id": job_id}, result)


def add_parameters(job_id, gtf, fasta, lod_score, kmer_complex, sd, total_sites, wobbles, reverse, species, mirna):
    # Connect to database
    client = MongoClient(host)
    mydb  = client[db]
    mydb.authenticate(db_user, db_pass)

    result = mydb['queries'].find_one({"job_id": job_id})

    result['gtf'] = gtf.split("/")[-1]
    result['fasta'] = fasta.split("/")[-1]
    result['lod_score'] = lod_score
    result['kmer_complex'] = kmer_complex
    result['sd'] = sd
    result['total_sites'] = total_sites
    result['wobbles'] = wobbles
    result['reverse'] = reverse
    result['species'] = species
    result['mirna'] = mirna

    # Update collection
    mydb['queries'].update({"job_id": job_id}, result)


def removeCollection(job_id):

    # Connect to database
    client = MongoClient(host)
    mydb  = client[db]
    mydb.authenticate(db_user, db_pass)

    # Remove from queries
    mydb['queries'].remove({"job_id": job_id})
    # Remove from annotation
    mydb['annotation'].remove({"job_id": job_id})
    # Remove from sponges
    mydb['sponges'].remove({"job_id": job_id})
    # Remove from conservation
    mydb['conservation'].remove({"job_id": job_id})


# CLEAN DIRECTORY AND ¿DATABASE?
def clean_job(job_id, database, logs):

    # Clean files
    shutil.rmtree(FILES_PATH + "/" + job_id)

    # Remove logs
    if logs is True:
        shutil.rmtree(LOGS_PATH + "/" + job_id)

    if database is True:
        removeCollection(job_id)


# JOBS
def create_job(command, stdout, stderr, mem):
    s = drmaa.Session()
    s.initialize()
    jobid = -1
    try:
        jt = s.createJobTemplate()
        jt.remoteCommand = command
        jt.nativeSpecification = "--mincpus=1 --mem=" + str(mem)
#        jt.email = ["pfurio@cipf.es"]
        jt.joinFiles = True
        jt.outputPath = stdout
        jt.errorPath  = stderr

        jobid = s.runJob(jt)
        s.deleteJobTemplate(jt)

    finally:
        s.exit()

    return jobid


def waitForJobs(job_ids):

    s = drmaa.Session()
    s.initialize()
    retVal = []
    try:
        # Wait for all jobs to finish
        s.synchronize(job_ids, drmaa.Session.TIMEOUT_WAIT_FOREVER, False)

        for curjob in job_ids:
            # Obtain finish status of the jobs
            retVal.append( s.wait(curjob, drmaa.Session.TIMEOUT_WAIT_FOREVER) )
    finally:
        s.exit()

    return retVal


def my_pipeline(job_id, download_gtf, download_fasta, gtf, fasta, lod_score, kmer_complex, sd, total_sites, wobbles, reverse, species, email, mirna, expression):

    # Create directory to contain output and error files
    os.makedirs(LOGS_PATH + "/" + job_id)

    print strftime("%a, %d %b %Y %H:%M:%S") + " A new job has been submitted: " + job_id

    if PROXY_ENABLE is True:
        socks.setdefaultproxy(socks.PROXY_TYPE_HTTP, PROXY_HOST, PROXY_PORT)
        socket.socket = socks.socksocket

    # 0.1. Send email
    send_email(email,job_id,"","newjob")

    try:
        # 0. If we have to download the gtf from ensembl, download it
        if download_gtf is True:
            print strftime("%a, %d %b %Y %H:%M:%S") + " Downloading GTF file from Ensembl: " + gtf
            gtf_file_name = gtf.split("/")[-1]
            urllib.urlretrieve("ftp://" + gtf, FILES_PATH + "/" + job_id + "/" + gtf_file_name)

            # Now decompress the file
            print strftime("%a, %d %b %Y %H:%M:%S") + " Decompressing GTF file..."
            filein  = gzip.open(FILES_PATH + "/" + job_id + "/" + gtf_file_name, 'rb')
            # Set the path to the final decompressed GTF file
            gtf = FILES_PATH + "/" + job_id + "/" + gtf_file_name[:-3]
            fileout = open(gtf, 'wb')
            fileout.write(filein.read())
            filein.close()
            fileout.close()

            # Remove the compressed file
            os.remove(FILES_PATH + "/" + job_id + "/" + gtf_file_name)

        # 0. If we have to download the fasta file from ensembl, download it and filter by biotype
        if download_fasta is True:
            print strftime("%a, %d %b %Y %H:%M:%S") + " Downloading FASTA file from Ensembl: " + fasta
            fasta_file_name = fasta.split("/")[-1]
            urllib.urlretrieve("ftp://" + fasta, FILES_PATH + "/" + job_id + "/" + fasta_file_name)
            #prefasta = FILES_PATH + "/" + job_id + "/" + fasta.split("/")[-1][:-3] + ".prefiltered"
            fasta = FILES_PATH + "/" + job_id + "/" + fasta.split("/")[-1][:-3]

            # Now decompress the file
            print strftime("%a, %d %b %Y %H:%M:%S") + " Decompressing FASTA file..."
            filein  = gzip.open(FILES_PATH + "/" + job_id + "/" + fasta_file_name, 'rb')
            # Set the path to the final decompressed FASTA file
            #fileout = open(prefasta, 'wb')
            fileout = open(fasta, 'wb')
            fileout.write(filein.read())
            filein.close()
            fileout.close()

            # Now filter the file
            #print strftime("%a, %d %b %Y %H:%M:%S") + " Filtering FASTA file..."
            #os.system("python /data/conesa/spongescan/webserver/cdna_parser.py -i " + prefasta + " -o " + fasta)
            # Remove the prefiltered fasta file
            #os.remove(prefasta)
            os.remove(FILES_PATH + "/" + job_id + "/" + fasta_file_name)
    except:
        send_email(email,job_id,"Error downloading fasta or GTF file from Ensembl","error")
        # TODO: Remove all the folders (ROLLBACK)

    # Lock DRMAA
    lock.acquire()
    try:

        job_ids = []

        # Send jobs to get the miRNA files ...
        print strftime("%a, %d %b %Y %H:%M:%S") + "Obtaining miRNA lists and running predictions..."
        for i in range(8,5,-1):

            # Send jobs to get miRNA files
            mirna_file = FILES_PATH + "/" + job_id + "/mirna_" + str(i) + "_whitelist.txt"
            get_miRNA_sequences(mirna, i, mirna_file)
            my_cmd = create_predictor_cmd(job_id, mirna_file, gtf, fasta, i, lod_score, kmer_complex, sd, total_sites, wobbles, reverse)
            stdout = ":" + LOGS_PATH + "/" + job_id + "/predictor_" + str(i) + ".out"
            stderr = ":" + LOGS_PATH  + "/" + job_id + "/predictor_" + str(i) + ".err"
            if i == 6 and wobbles == "false":
                mem = 5000
            elif i == 6 and wobbles == "true":
                mem = 7500
            elif i == 7 and wobbles == "false":
                mem = 8000
            elif i == 7 and wobbles == "true":
                mem = 15000
            elif i == 8 and wobbles == "false":
                mem = 12000
            else:
                mem = 30000
            job_ids.append(create_job(my_cmd, stdout, stderr, mem))

        for job in job_ids:
            if job == -1:
                # Remove everything and close
                clean_job(job_id, False, False)
                raise Exception("ERROR: There was some kind of error sending the jobs to the queue")

        # 3. Create DB with current status
        update_status(job_id, email, "running", "1. Predictions are running")
        add_parameters(job_id, gtf, fasta, lod_score, kmer_complex, sd, total_sites, wobbles, reverse, species, mirna)

        print strftime("%a, %d %b %Y %H:%M:%S") + "Waiting for subjobs running predictions in job " + job_id + ". Subjobs: "+ ",".join(job_ids)

        # 4. Wait for the jobs
        exit_status = waitForJobs(job_ids)

        for value in exit_status:
            if value.exitStatus != 0:
                # Job didn't finish successfully
                update_status(job_id, email, "error", "There was an error running the predictions.")
                send_email(email, job_id, "An error was found running the predictions", "error")
                # Remove everything and close
                clean_job(job_id, True, False)
                raise Exception("ERROR: There was some kind of error running the predictions")


        # lncRNA expression file
        job_ids = []
        if expression[0] is not None:
            print strftime("%a, %d %b %Y %H:%M:%S") + "Storing expression values for lncRNA data..."
            my_cmd = create_expression_cmd(job_id, 'lncRNA', expression[0])
            stdout = ":" + LOGS_PATH  + "/" + job_id + "/expr_lncrna.out"
            stderr = ":" + LOGS_PATH  + "/" + job_id + "/expr_lncrna.err"
            job_ids.append(create_job(my_cmd, stdout, stderr, 2000))

        # miRNA expression file
        if expression[1] is not None:
            print strftime("%a, %d %b %Y %H:%M:%S") + "Storing expression values for miRNA data..."
            my_cmd = create_expression_cmd(job_id, 'miRNA', expression[1])
            stdout = ":" + LOGS_PATH  + "/" + job_id + "/expr_mirna.out"
            stderr = ":" + LOGS_PATH  + "/" + job_id + "/expr_mirna.err"
            job_ids.append(create_job(my_cmd, stdout, stderr, 2000))            

        if len(job_ids) > 0:
            print strftime("%a, %d %b %Y %H:%M:%S") + "Waiting for job " + job_id + ". Subjobs: "+ ",".join(job_ids)

            # 4. Wait for the jobs
            exit_status = waitForJobs(job_ids)

            for value in exit_status:
                if value.exitStatus != 0:
                    # Job didn't finish successfully
                    update_status(job_id, email, "warning", "There was an error when storing the expression data.")
                    # Remove everything and close
                    print strftime("%a, %d %b %Y %H:%M:%S") + "There was an error when storing expression information." 
                    send_email("pedrofurio@gmail.com", job_id, "An error was found storing the expression data.", "error")
                    raise Exception("ERROR: There was an error when storing the expression data.")

        clean_job(job_id, False, True)
        update_status(job_id, email, "ready", "")
        send_email(email, job_id, "", "finished")
        print strftime("%a, %d %b %Y %H:%M:%S") + "Job " + job_id + " has finished."

    except:
        clean_job(job_id, True, False)
        update_status(job_id, email, "error", "There was an error with the computation.")
        send_email(email, job_id, "An error was detected when computing the predictions.", "error")
        send_email("pedrofurio@gmail.com", job_id, "An error was detected when computing the predictions.", "error")

    finally:
        lock.release()

    return


# download flag True to download GTF file from Ensembl, False if the user uploaded their own GTF file
def submit_new_job(job_id, download_gtf, download_fasta, gtf, fasta, lod_score, kmer_complex, sd, total_sites, wobbles, reverse, species, email, mirna, expression):
# download, gtf_file, fasta_file, lod_score, kmer_complex, sd, total_sites, wobbles, reverse_fasta, species_gtf, mail)

    try:
        t = threading.Thread(target=my_pipeline,
                             args=(job_id, download_gtf, download_fasta, gtf, fasta, lod_score, kmer_complex, sd, total_sites, wobbles,
                                   reverse, species, email, mirna, expression))
        t.start()
    except:
        sys.stderr.write('There was an error submitting job ' + job_id + '\n')
        return False

    return True
