import os, sys
sys.path.append(os.path.dirname(__file__))

os.environ['DRMAA_LIBRARY_PATH'] = '/data/apps/slurm-drmaa/1.2.0/lib/libdrmaa.so'

from main import app as application
