# -*- coding: utf-8 -*-

import sys
import os
import getopt

# DATABASE
from time import gmtime, strftime
from datetime import datetime, timedelta

from pymongo import MongoClient

db_user = "lncscanadmin"
db_pass = "mrVRTDa4"
host    = "pubdb1"
db      = "lncscan"


def main():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "cj:", ["check", "jobID="])
    except getopt.GetoptError as err:
        print(err) # will print something like "option -a not recognized"
        usage()
        sys.exit(2)

    for o, a in opts:
        if o in ("-c","--check"):
            checkDays()
        elif o in ("-j", "--jobID"):
            removeCollection(a)
        else:
            usage()
            sys.exit(2)
    if len(opts) == 0:
        usage()

def usage():
    print "\nUsage: python removeJobsFromMongo.py [options]"
    print "Options:"
    print "\t-c, --check:\n\t\t Set if you want to check and remove all the jobs older than 3 months."
    print "\t-j, --jobID:\n\t\t Specify the job ID to be removed from the mongo database"

def removeCollection(job_id):
    # Connect to database
    client = MongoClient(host)
    mydb  = client[db]
    mydb.authenticate(db_user, db_pass)

    # Remove from queries
    mydb['queries'].delete_one({"job_id": job_id})
    # Remove from annotation
    mydb['annotation'].delete_one({"job_id": job_id})
    # Remove from sponges
    mydb['sponges'].delete_one({"job_id": job_id})


def checkDays():
    # Connect to database
    client = MongoClient(host)
    mydb  = client[db]
    mydb.authenticate(db_user, db_pass)

    oldPrediction = datetime.now() - timedelta(days=90)
    old = oldPrediction.strftime('%y%m%d')

    results = mydb['queries'].find({"day": {"$lt":  old}})
    cont = 0
    for result in results:
        cont += 1
        removeCollection(result["job_id"])
    print str(cont) + " jobs were removed from the database"

if __name__ == "__main__":
    main()
