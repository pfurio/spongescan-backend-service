from pymongo import MongoClient
from bson.json_util import dumps, loads
import csv
import time
import numpy
import sys
import json
import os

credentials = json.load(open(os.path.dirname(__file__) + "/mongo_credentials.json",'r'))
db_user = credentials['users']['readonly']['user']
db_pass = credentials['users']['readonly']['pass']
host = credentials['host']
db   = credentials['database']

def createFilterMongo(myfilters):
    myOutput = {}
    allFilters = loads(myfilters)
    for myfilter in allFilters:
        _oper  = myfilter["operator"]
        _key   = myfilter["property"]
        _value = myfilter["value"]
        if _oper == "like": # String
             myOutput[_key] = {"$regex": _value, "$options": "i"}
        else: # Numeric
            if _oper != "eq":
                if _key in myOutput:
                    aux = myOutput[_key]
                    aux["$" + _oper] = float(_value)
                else:
                    myOutput[_key] = {"$" + _oper: float(_value)}
            else:
                myOutput[_key] = float(_value)
    return myOutput


def queryMongoDB(jobID, kmer_size, known, myfilter):
    client = MongoClient(host)
    mydb  = client[db]
    mydb.authenticate(db_user, db_pass)
    myQuery = {}
    if myfilter is not None:
        myQuery = createFilterMongo(myfilter)
    myQuery["job_id"] = jobID
    myQuery["kmer_size"] = int(kmer_size)
    if "mirnas" not in myQuery or known != "1":
        if known == "1":
            myQuery["mirnas"] = {"$exists": True}
        else:
            myQuery["mirnas"] = {"$exists": False}
    sp_results = mydb["sponges"].find(myQuery, {"job_id": 0, "_id": 0}) # Query to sponges
    query_result = mydb['queries'].find_one({"job_id": jobID})          # Query to queries

    return [sp_results, query_result]

# This function queries the database to find all putative sponges and return them to the client in a json format
def retreiveJob(jobID, kmer_size, known, start, limit, tosort, direct, myfilter):
    t_start = time.time()
    [sp_results, query_result] = queryMongoDB(jobID, kmer_size, known, myfilter)
    
    # Skip if we have to start later
    direction = 1 if direct == 'ASC' else -1
    total = sp_results.count()
    sp_results.sort([(tosort, direction)])
    sp_results.skip(start)
    sp_results.limit(limit)

    # Vector we are going to return
    myresults = []

    for result in sp_results:
        toreport = result
        length = 0
        for exon in result["exons"]:
            length = length + exon['end'] - exon['start'] + 1
        toreport.update({"length": length})

        if "conservation" in query_result and query_result["conservation"] == 1:
            # Check conservation scores
            cons_result = mydb["conservation"].find_one({"job_id": jobID},{"cons": {"$elemMatch": {"trans_id": result['trans_id']}}})
            try:
                toreport.update({"cons_graph": cons_result['cons'][0]["graph"]})
            except:
                pass

        if "accessibility" in query_result and query_result["accessibility"] == 0:
            # Check accessibility scores
            acc_result = mydb["accessibility"].find_one({"job_id": jobID},{"acc": {"$elemMatch": {"trans_id": result['trans_id']}}})
            try:
                toreport.update({"acc_graph": acc_result['acc'][0]["graph"]})
            except:
                pass

        myresults.append(toreport)

    return {"success": True, "totalCounts": total, "response": myresults, "time": "{0:0.3f}".format(time.time()-t_start)}


def getArguments (job_id):
    client = MongoClient(host)
    mydb = client[db]
    mydb.authenticate(db_user, db_pass)
    result = mydb['queries'].find_one({"job_id": job_id})
    salida = {}

    salida['gtf'] = result['gtf']
    salida['fasta'] = result['fasta']
    salida['lod_score'] = float(result['lod_score'])
    salida['kmer_complex'] = float(result['kmer_complex'])
    salida['sd'] = float(result['sd'])
    salida['total_sites'] = int(result['total_sites'] )
    salida['wobbles'] = result['wobbles'] 
    salida['reverse'] = result['reverse']
    salida['species'] = result['species']
    salida['mirna'] = result['mirna']

    return salida


# This function queries the database to check if the specified job_id has finished
def jobStatus(job_id):

    client = MongoClient(host)
    mydb = client[db]
    mydb.authenticate(db_user, db_pass)
    result = mydb['queries'].find_one({"job_id": job_id})

    if result is None:
        salida = {"success": False, "msg": "ERROR: Job not found in the database"}
    else:
        if result['status'] == "ready":
            salida = {"success": True}
        else:
            msg = result['status'].upper() + ": " + result['msg']
            salida = {"success": False, "msg": msg}
    return salida

# This function queries the database to check if there's any job with that job_id
def existsJob(job_id):

    client = MongoClient(host)
    mydb = client[db]
    mydb.authenticate(db_user, db_pass)
    result = mydb['queries'].find_one({"job_id": job_id})

    if result is None:
        return False
    else:
        return True

# This function queries the database to return all the detected sponges within the specified range
def getRegions(job_id, positions, kmer_size):

    t_start = time.time()

    client = MongoClient(host)
    mydb = client[db]
    mydb.authenticate(db_user, db_pass)

    # an_db = mydb["annotation"]
    sp_db = mydb["sponges"]
    
    mis_responses = []
    mis_positions = positions.split(",")
    for position in mis_positions:

        print job_id + ": Asked region " + position

        unique_kmers = set()
        all_kmers = []
        chrom = position.split(":")[0]
        chrom_chr = chrom
        start = int(position.split(":")[1].split("-")[0])
        end   = int(position.split(":")[1].split("-")[1])
        result = sp_db.find({"$and": [ {"chrom": chrom_chr}, {"trans_start": {"$lt": end}}, {"trans_end": {"$gt": start}}, {"kmer_size": int(kmer_size)}, {"job_id": job_id}]})
        for result_i in result:
            kmer = result_i["kmer"]
            for position in result_i['positions']:
                # realPosition will contain the real coordinates of the kmer
                realPosition = position 
                if result_i['strand'] == "+":
                    tamanyo = 0
                    for exon in result_i['exons']:
                        exonLength = int(exon['end']) - int(exon['start']) + 1
                        if tamanyo + exonLength > position: # The binding is in this exon
                            realPosition = position - tamanyo + int(exon['start'])
                            break
                        tamanyo += exonLength
                else:
                    tamanyo = 0
                    for exon in result_i['exons']:
                        exonLength = int(exon['end']) - int(exon['start']) + 1
                        if tamanyo + exonLength > position:
                            realPosition = int(exon['end']) - position + tamanyo
                            break
                        tamanyo += exonLength
                        
                mystart = realPosition
                myend   = realPosition + int(kmer_size) - 1
                aux     = str(mystart) + "_" + str(myend)

                if aux not in unique_kmers:
                    all_kmers.append({"id": kmer, "name": kmer, "chromosome": chrom, "start": mystart, "end": myend, "strand": result_i['strand']})
                    unique_kmers.add(aux)

        mis_responses.append({"id": chrom + ":" + str(start) + "-" + str(end), "numResults": str(len(all_kmers)), "result": all_kmers})

    return {"response": mis_responses, "time": "{0:0.3f}".format(time.time()-t_start) }

# {
#     success:
#     response: [{
#         gene_id:
#         found: (True or False)
#         expr_format:
#         expression: [{
#             sample:
#             value:
#         }..{}]
#     }...{}]
# }
def getExpression(job_id, gene_ids):

    t_start = time.time()

    all_gene_ids = gene_ids.split(",")

    myresponse = {"success": True, "response": []}

    client = MongoClient(host)
    mydb = client[db]
    mydb.authenticate(db_user, db_pass)

    for gene_i in range(len(all_gene_ids)):
        gene_id = all_gene_ids[gene_i]
        if gene_i == 0:
            biotype = "lncRNA"
        else:
            biotype = "miRNA"

        result = mydb['expression'].find_one({"job_id": job_id, "biotype": biotype},{"genes": {"$elemMatch": {"gene_id": gene_id}}, "expr_fields": 1, "tissues": 1, "expr_format": 1})

        try:
            n_fields = len(result['expr_fields'])
    
            my_tissues_expr = {}
            my_tissues_format = {}
            for i in range(n_fields):
                tejido = result['tissues'][i]
                sample = result['expr_fields'][i]
                expres = result['genes'][0]['expr_values'][i]

                if tejido not in my_tissues_format:
                    my_tissues_expr[tejido] = []
                    my_tissues_format[tejido] = []
                my_tissues_format[tejido].append([sample, expres])
                my_tissues_expr[tejido].append(expres)


            tissueExpr    = []
            drilldownExpr = []
            
            my_tissues_sorted = my_tissues_expr.keys()
            my_tissues_sorted.sort()

            for tejido in my_tissues_sorted:

                # Obtain the median of the expression values
                tissueExpr.append({"name": tejido, "drilldown": tejido, "y": numpy.max(my_tissues_expr[tejido])})
                drilldownExpr.append({"id": tejido, "data": my_tissues_format[tejido]})


            myresponse['response'].append({ "gene_id": gene_id, "found": True, "expr_format": result['expr_format'], "tissueExpr": tissueExpr, "drillDownExpr": drilldownExpr})

        except:
            print "Unexpected error:", sys.exc_info()[0]
            # Add the current gene_id but not the expression values
            myresponse['response'].append({"gene_id": gene_id, "found": False})

    myresponse['time'] = "{0:0.3f}".format(time.time()-t_start)
    
    return myresponse


def createCSV(jobID, kmer_size, known):
    t_start = time.time()
    [sp_results, query_result] = queryMongoDB(jobID, kmer_size, known, None)
    sp_results.sort([("sd", 1)])

    yield "GeneID;GeneName;Chrom;Start;End;Strand;TransID;TransName;TransStart;TransEnd;Biotype;Kmer;miRNAs;" \
          "Positions;LODScore;StdDev;ComplScore;TotalOccur;MedianDist;WindowSize;OccurInWindow\n"

    for result in sp_results:
        mysalida = result["gene_id"] + ";" + result["gene_name"] + ";" + result["chrom"] + ";" + str(result["start"]) + ";" \
                  + str(result["end"]) + ";" + result["strand"] + ";" + result["trans_id"] + ";" + result["trans_name"] + ";" \
                  + str(result["trans_start"]) + ";" + str(result["trans_end"]) + ";" \
                  + result["biotype"] + ";" + result["kmer"] + ";"
        if known == "1":
            mysalida += ', '.join(result["mirnas"])
        mysalida += ";" + ', '.join(str(x) for x in result["positions"]) + ";" \
                  + str(result["lod_score"]) + ";" + str(result["sd"]) + ";" + str(result["compl_score"]) + ";" + str(result["total"]) + ";" \
                  + str(result["median_distance"]) + ";" + str(result["w_size"]) + ";" + str(result["occ"]) + "\n"
        yield mysalida
